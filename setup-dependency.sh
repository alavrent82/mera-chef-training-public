#!/bin/bash -e

# Test if curl is installed
type curl >/dev/null 2>&1 || { echo >&2 "'curl' is not installed, but required.  Aborting."; exit 1; }

# Test if unzip is installed
type unzip >/dev/null 2>&1 || { echo >&2 "'unzip' is not installed, but required.  Aborting."; exit 1; }

curl -sL https://github.com/hakimel/reveal.js/archive/master.zip -o master.zip
unzip -q master.zip
mv reveal.js-master reveal.js
rm master.zip

echo 'Dependency has been setup'
exit 0

